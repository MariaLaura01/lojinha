<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Lojinha do Z� - Login</title>
	<link rel="stylesheet" href="css/login.css">
</head>

<body>
	<div class="main">
		<h1>Lojinha do Z�</h1>
		
		<form action="login" method="post">
			<fieldset>
				<legend>Login</legend>
				
				<p>
				<label for="username">Usu�rio: </label>
				<input type="text" id="username" name="username" min="3" required placeholder="username">
				</p>
				
				<p>
				<label for="password">Senha: </label>
				<input type="password" id="password" name="password" required placeholder="password">
				</p>
				
				<button type="submit">Enviar</button>
			</fieldset>
		</form>
	</div>
<% 
	String erro_msg = String.valueOf(request.getAttribute("erro"));
	
	if(erro_msg != null)
	{
		%>
			<p class="erro"><%= erro_msg%></p>
		<%
	}

%>
</body>
</html>